class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.text :desc
      t.integer :Servicetype_id

      t.timestamps
    end
  end
end
