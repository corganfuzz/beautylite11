class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.date :date
      t.integer :Customer_id
      t.boolean :is_done

      t.timestamps
    end
  end
end
