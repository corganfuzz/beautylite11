class CreateItemlines < ActiveRecord::Migration
  def change
    create_table :itemlines do |t|
      t.integer :order_id
      t.integer :Stylistserv_id

      t.timestamps
    end
  end
end
