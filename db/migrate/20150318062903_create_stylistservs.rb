class CreateStylistservs < ActiveRecord::Migration
  def change
    create_table :stylistservs do |t|
      t.integer :Stylist_id
      t.integer :Service_id

      t.timestamps
    end
  end
end
