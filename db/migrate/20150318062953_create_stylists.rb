class CreateStylists < ActiveRecord::Migration
  def change
    create_table :stylists do |t|
      t.string :fname
      t.string :lname
      t.string :phone

      t.timestamps
    end
  end
end
