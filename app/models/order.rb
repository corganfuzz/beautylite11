class Order < ActiveRecord::Base
  belongs_to :Customer
  has_many :Itemlines
  has_many :Stylistserv, :through => :Itemlines
end
