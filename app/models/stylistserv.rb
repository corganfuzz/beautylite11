class Stylistserv < ActiveRecord::Base
  belongs_to :Stylist
  belongs_to :Service
  has_many :Orders, :through => :Itemlines
end
