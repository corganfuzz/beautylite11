class Service < ActiveRecord::Base
  belongs_to :Servicetype
  has_many :Stylists, :through => :Stylistservs
  has_many :Stylistservs
end
