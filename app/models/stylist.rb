class Stylist < ActiveRecord::Base
  has_many :Stylistservs
  has_many :Services, :through => :Stylistservs
end
